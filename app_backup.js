var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});


io.on('connection', function(socket){
  	console.log('a user connected');

  	socket.on('chop', function(data){
		  socket.broadcast.emit('updateEnemy', data);
 	    //io.emit('newBranch', Math.floor(Math.random() * 3));
	 });

	socket.on('lose', function(){
		socket.broadcast.emit('win');
	});

  
  /*
  **  exactly 2 players
  */
  if(Object.keys(io.engine.clients).length==2){
    io.emit('allPlayersReady');
    io.sockets.emit('tree', createTree());
  /*    
  ** too many ppls - sorry, u are disconnected boi
  */
  }else if(Object.keys(io.engine.clients).length>2){
    socket.emit('full');
    socket.disconnect();
  }



});

/*
** create tree to send to folks who connect
*/
function createTree(){
  var tree = new Array();
  for(var i = 1; i < 500; i += 2){
    tree[i] = Math.floor(Math.random() * 3);
    tree[i+1] = 1;
  }
  tree[0] = 1;
  return tree;
}

http.listen(3000, function(){
  console.log('listening on *:3000');
});

