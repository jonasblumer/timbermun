var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});


io.on('connection', function(socket){
  	console.log('a user connected');

  	socket.on('chop', function(data){
		  socket.broadcast.emit('updateEnemy', data);
 	    //io.emit('newBranch', Math.floor(Math.random() * 3));
	 });

	socket.on('lose', function(){
		socket.broadcast.emit('win');
    setTimeout(function(){
      startNewGame(socket);
    }, 3000);
	});

  console.log(Object.keys(io.engine.clients).length);
  
  /*
  **  exactly 2 players
  */
  if(Object.keys(io.engine.clients).length==2){

   startNewGame(socket);

  /*    
  ** too many ppls, sorry
  */
  }else if(Object.keys(io.engine.clients).length>2){
    socket.emit('full');
    socket.disconnect();
  }
});

function startNewGame(socket){
  var timer = 5;
  io.sockets.emit('tree', createTree());
  
  var counter = setInterval(function(){       
    //$('#winner').html(i++);
    io.sockets.emit('timer', timer--);
    if(timer < 0){
      io.emit('allPlayersReady');
        //isGameStarted = true;  
        //console.log('game started');
        //$('#winner').html('');
      
      clearInterval(counter);
    } 
  }, 1000);

  socket.on('disconnect', function(){
    io.sockets.emit('disconnected');
  });
}

/*
** create tree to send to folks who connect
*/
function createTree(){
  var tree = new Array();
  for(var i = 1; i < 500; i += 2){
    tree[i] = Math.floor(Math.random() * 3);
    tree[i+1] = 1;
  }
  tree[0] = 1;
  return tree;
}

http.listen(3000, function(){
  console.log('listening on *:3000');
});

